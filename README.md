
### DIESER FORK VON AIRCHAT IST NOCH NICHT FUER DEN AMATERUFUNK GEEIGNET ###


---

### VIDEO:  https://vimeo.com/92651272
[![IAirChat Video - Free Communications for Everyone](http://i.vimeocdn.com/video/472437206_960.jpg)](https://vimeo.com/92651272)

---


##Quick Start
###Software Setup

####FreeBSD 10
From a fresh server install:
```
# pkg install make
# pkg install perl-5.16.xx
# perl install-modules-airchat-freebsd.pl
```

then...
```
# cpanp
CPAN Terminal> i Net::Server --skiptest
CPAN Terminal> i HTTP::Server::Simple::CGI::PreFork
```

That will get you the airchat server running,
keep in mind installing fldigi requires a graphical environment aka X
so:

1.  you setup airchat to connect with a remote station running fldigi
2.  install X and then:

 ```# pkg install fldigi-3.xx.xx```


####Windows
Install Strawberry Perl >= 5.18 (the portable zip version fits well for example)
from [http://strawberryperl.com/](http://strawberryperl.com/download/)

Once you get Perl installed, run in your Perl shell:
```# perl install-modules-airchat-windows.pl```

Then install these modules via the cpanplus terminal:
```
# cpanp
CPAN Terminal> i Net::Server --skiptest
CPAN Terminal> i HTTP::Server::Simple::CGI::PreFork --skiptest
```

Then install fldigi from: http://www.w1hkj.com/download.html

####Linux (Debian / tested also on Ubuntu Trusty)
Install some needed stuff:

```
# apt-get install make libcpanplus-perl libhttp-server-simple-perl libcrypt-cbc-perl libcrypt-rijndael-perl librpc-xml-perl libxml-feedpp-perl liblwp-protocol-socks-perl libnet-twitter-lite-perl libnet-server-perl
```

There's an optional and commented `use Net::SSLGlue::LWP` before `use LWP::UserAgent` on [airchat.pl](../blob/master/airchat.pl#L14) which can be installed with `# apt-get install libnet-sslglue-perl`. This magically fixes LWP for https requests issues, when for example you want to include feeds only available via proxy to a HTTPS address. If you don't have the updated `libwww-perl 6.05-2` and `liblwp-protocol-https-perl 6.04-2` available from repositories (should be available from the jessie repos though) but...

We strongly recommend you look to update `libwww-perl` and `liblwp-protocol-https-perl` to their latest versions, cause using `SSLGlue` will eventually break https access to the Twitter API.

Check if you have updated packages for `libnet-twitter-lite-perl` because you will need the Twitter API v1.1 support.
```
# perl install-airchat-modules-linux.pl
```

Which will install `HTTP::Server::Simple::CGI::PreFork` (needed) and `Net::Twitter::Lite::WithAPIv1_1`

If you want to install *Fldigi* on the same machine as Airchat then:

```
# apt-get install fldigi
```

(running fldigi requires a graphical environment)


####MacOS X
* Get XCode.
* Launch XCode and bring up the Preferences panel. Click on the Downloads tab. Click to install the Command Line Tools. Check you got 'make' installed.
run:

```
# perl install-modules-airchat-macosx.pl
# cpanp
CPAN Terminal> i Net::Server --skiptest
CPAN Terminal> i HTTP::Server::Simple::CGI::PreFork --skiptest
```

##General Notes
Airchat runs by default on port 8080, connect your browser to http://localhost:8080. 

####READ THE CODE.
If you find some problem running AirChat, please try updating modules and linked libraries. we've found some issues related to outdated implementations. (like '500 Bad arg length for Socket6::unpack_sockaddr_in6, length is 16, should be 28'happening in Ubuntu Precise when enabling the Twitter gateway).

###Fldigi Setup
run `fldigi`.
skip everything if you want but you must configure audio devices to make it work with your capture device and your audio output device. Test if it's working capturing audio signals and playing audio and that's all.

(Note: keep your fldigi updated always)

